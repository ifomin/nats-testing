import {logger} from '../common/logger'
import { EventEmitter } from 'events';
import {Bot} from "../../types";
import {NatsManager} from "../common/nats";
import {BotUnit} from "./BotsUnit";


export class BotsManager extends EventEmitter{
  private bots: BotUnit[] = [];

  constructor(private nats:NatsManager) {
    super();
  }

  public async setup(botsStructures: Bot[]) {
    const usedUIDS = [];
    for (const botStruct of botsStructures) {
      usedUIDS.push(botStruct.uid);
      await this.addBot(botStruct);
    }
    await this.removeUnusedBots(usedUIDS);
  }

  public async addBot(bot: Bot) {
    let botUnit = this.bots.filter(it=> it.data.uid === bot.uid)[0];
    if (!botUnit) {
      botUnit = new BotUnit(bot);
      await botUnit.setup(this.nats);
      console.log('emit');
      this.emit('new_bot', botUnit.data);
      this.bots.push(botUnit);
    }
    return botUnit
  }

  private async removeUnusedBots(usedUIDS: string[]) {
    const unusedBots = this.bots.filter((bot) => !usedUIDS.includes(bot.data.uid));
    for (const bot of unusedBots) {
      await this.removeBot(bot.data.uid);
    }
  }

  private async removeBot(uid: string): Promise<void> {
    const botIndex = this.bots.findIndex((bot) => bot.data.uid === uid);
    if (botIndex !== -1) {
      const bot = this.bots[botIndex];
      await bot.stop();
      this.bots.splice(botIndex, 1);
    }
  }
}