import {Bot, Update, UpdateType} from "../../types";
import {MessageContext, VK} from "vk-io";
import {logger} from "../common/logger";
import {NatsManager} from "../common/nats";

export class BotUnit {
  public data: Bot;
  private readonly vk: VK;

  constructor(data: Bot) {
    this.data = data;
    this.vk = new VK({
      token: data.token,
      pollingGroupId: +data.group_id,
      apiMode: 'parallel',
    });
  }

  public async stop() {
    await this.vk.updates.stop();
  }

  public async setup(nats: NatsManager) {
    if (!this.data.pulling) return this.vk;
    await this.vk.updates
      .on('wall_repost', async (ctx) => {
        if (!ctx.isRepost || ctx.type !== 'wall_post') {
          return;
        }
        const copyHistory = ctx.wall.copyHistory;
        if (!copyHistory) {
          return;
        }
        const postId = copyHistory[0].id;
        const isPost = copyHistory[0].postType === 'post';

        if (!postId || !isPost) {
          return;
        }
        await nats.publish<Update>(`updates_${this.data.uid}`, {
          user: ctx.wall.ownerId,
          type: UpdateType.repost,
          object: postId,
        });
        logger.info(`${this.data.uid}> publish ${JSON.stringify(this.data)}`);
      })
      .on('message_new', async (ctx) => {
        if (ctx.is(['message'])) {
          if (ctx.isOutbox || ctx.isFromGroup) {
            return;
          }
        }

        await nats.publish<Update>(`updates_${this.data.uid}`, {
          user: ctx.senderId,
          type: UpdateType.message,
          text: ctx.text,
        });
        logger.info(`${this.data.uid}> publish ${JSON.stringify(this.data)}`);
      })
      .on('like_add', async (ctx: MessageContext) => {
        if (ctx.objectType !== 'post' || ctx.objectId < this.data.from_id) {
          return;
        }
        await nats.publish<Update>(`updates_${this.data.uid}`, {
          user: ctx.likerId,
          type: UpdateType.like,
          object: ctx.objectId
        });
        logger.info(`${this.data.uid}> publish ${JSON.stringify(this.data)}`);
      })
      .on('wall_reply_new', async (ctx) => {
        if (ctx.fromId === -this.data.group_id || !ctx.isWallComment || ctx.objectId < this.data.from_id) {
          return;
        }
        await nats.publish<Update>(`updates_${this.data.uid}`, {
          user: ctx.fromId || 0,
          type: UpdateType.comment,
          object: ctx.objectId,
          text: ctx.text,
          reply_to_comment: ctx.id
        });
        logger.info(`${this.data.uid}> publish ${JSON.stringify(this.data)}`);
      })
      .startPolling()

    logger.info(`${this.data.uid}> Long Polling started`);
    return this.vk;
  }
}