import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

@Entity('bots')
export class BotEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255, nullable: false })
  name: string;

  @Column({ type: 'varchar', length: 255, nullable: false })
  uid: string;

  @Column({ type: 'varchar', length: 255, nullable: false })
  token: string;

  @Column({ type: 'boolean', default: false })
  active: boolean;

  @Column({ type: 'int', nullable: false })
  group_id: number;

  @Column({ type: 'text', array: true, nullable: false })
  admins: string[];

  @Column({ type: 'int', nullable: false })
  from_id: number;

  @Column({ type: 'int', nullable: false })
  main_id: number;

}
