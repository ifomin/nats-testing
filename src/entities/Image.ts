import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

@Entity('images')
export class ImageEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 255, nullable: false })
  type: string;

  @Column({ type: 'varchar', length: 255 })
  url?: string;

  @Column({ type: 'bytea', nullable: false })
  data: Buffer;
}
