import {DB} from "./common/db";
import {logger} from './common/logger'
import {Bot, Comment, Message, Update} from "../types";
import {NatsManager} from "./common/nats";
import {VK, API} from "vk-io";
import {vkConfig} from "./common/config";
import cron from "node-cron";
import {BotsManager} from "./bots/BotsManager";

async function run() {
  const nats = new NatsManager();
  const botManager = new BotsManager(nats);

  const db = new DB();
  let sh;
  try {
    await nats.connect()
    logger.info('nats connected');

    await db.connect()
    logger.info('db connected');

    const sendMessage = async (bot: Bot, send: Message) => {
      const api = new API({
        token: bot.token,
        apiMode: 'sequential',
        apiRetryLimit: 3,
        apiTimeout: 10000,
      })
      try {
        await api.messages.send({
          user_id: send.user,
          dont_parse_links: send.dont_parse_link,
          message: send.text,
          attachment: send.attachments,
          keyboard: send.keyboard,
          random_id: send.random
        });
        logger.info(`subscribe.messages.${bot.uid}> ${send.user} ${send.text} - successfully`)
      } catch (e) {
        logger.error(`subscribe.messages.${bot.uid}> ${send.user} ${send.text} - error: ${e}`)
      }
    }

    botManager.on('new_bot', async (bot: Bot)=>{
      await nats.subscribe(`message.${bot.uid}`, async (message: Message) => {
        logger.info(`subscribe.comments.${bot.uid}> ${message.user} ${message.text}`)
        await sendMessage(bot, message);
      })
    });

    sh = cron.schedule('*/10 * * * * *', async () => {
      try {
        const bots = await db.getBots();
        await botManager.setup(bots)
      } catch (error) {
        logger.error(`Cron job error: ${error}`);
      }
    });
    sh.start();
  } catch (error) {
    logger.error(error);
    await db.close();
    nats.close();
  }
}

run().catch(console.error);
