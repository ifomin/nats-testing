import {DB} from "./common/db";
import {logger} from './common/logger'
import {Comment, Message, Update, UpdateType} from "../types";
import {NatsManager} from "./common/nats";
import {Keyboard} from "vk-io";

async function run() {
  const nats = new NatsManager();
  const db = new DB();
  try {
    await nats.connect()
    logger.info('nats connected');

    await db.connect()
    logger.info('db connected');

    const bots = await db.getBots();
    logger.info('bots loaded');

    for (const bot of bots) {
     await nats.subscribe(`updates.${bot.uid}.filtered`, async (update: Update)=>{
       logger.info(`subscribe.updates.${bot.uid}.filtered> ${update.type} ${update.user} ${update.object ? update.object : update.text}`)

       if (update.type === 'message') {

         const keyboard = Keyboard.builder().textButton({
           label: 'Погнали',
           color: Keyboard.PRIMARY_COLOR,
         }).toString();


         await nats.publish<Message>(`messages.${bot.uid}`, {
           user: update.user,
           text: `Ответ: ${update.text}`,
           object: update.object,
           dont_parse_link: true,
           keyboard,
           attachments: 'photo-200444977_457240653',
           random: Date.now()
         });

         return;
       }

       if (update.type === UpdateType.comment) {
         await nats.publish<Comment>(`comments.${bot.uid}`, {
           user: update.user,
           reply_to_comment: update.reply_to_comment || 0,
           text: `Ответ: ${update.text}`,
           object: update.object,
           attachments: 'photo-200444977_457240653',
           random: Date.now()
         });
       }
     })
    }
  } catch (error) {
    logger.error(error);
    nats.close();
    await db.close();
  }
}

run().catch(console.error);
