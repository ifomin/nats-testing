import {logger} from './common/logger'
import {Bot, Comment} from "../types";
import {NatsManager} from "./common/nats";
import {API} from "vk-io";
import {randItemNotPrev} from "./common/utils";
import cron from "node-cron";
import {BotsManager} from "./bots/BotsManager";
import {DB} from "./common/db";


const MAX_TOKENS_USE: number = 10;

async function run() {
  const nats = new NatsManager();
  const botManager = new BotsManager(nats);

  const db = new DB();
  let sh;
  try {
    await nats.connect()
    logger.info('nats connected');

    await db.connect()
    logger.info('db connected');

    const sendComment = async (bot: Bot, comment: Comment, admins?:any[]) => {
      let admin = randItemNotPrev(bot.admins, admins);
      try {
        if (!admins) {
          admins = [admin];
        } else {
          admins.push(admin);
        }
        const api = new API({
          token: admin,
          apiMode: 'sequential',
          apiRetryLimit: 3,
          apiTimeout: 10000,
        })
        await api.wall.createComment({
          message: comment.text,
          from_group: +bot.group_id,
          post_id: comment.object || 0,
          attachments: comment.attachments,
          reply_to_comment: comment.reply_to_comment,
          random_id: comment.random,
          owner_id: -bot.group_id
        });
        logger.info(`subscribe.comments.${bot.uid}> ${comment.user} ${comment.text} - successfully send`)
      } catch (e) {
        logger.error(`subscribe.comments.${bot.uid}> ${admin} - error ${e}`);

        const adminsSize = admins?.length || 0;
        if (bot.admins.length * 0.9 < (adminsSize || 0)
          || (adminsSize > MAX_TOKENS_USE)) {
          logger.error(`subscribe.comments.${bot.uid}> try resend limits all:${bot.admins.length} used:${admins?.length}`);
          return;
        }

        logger.info(`subscribe.comments.${bot.uid}> ${comment.user} - try resent`)
        setTimeout(async () => {
          await sendComment(bot, comment, admins)
        }, 200)

      }
    }

    botManager.on('new_bot', async (bot: Bot)=>{
      await nats.subscribe(`comments.${bot.uid}`, async (comment: Comment) => {
        logger.info(`subscribe.comments.${bot.uid}> ${comment.user} ${comment.text}`)
        await sendComment(bot, comment);
      })
    });

    sh = cron.schedule('*/10 * * * * *', async () => {
      try {
        const bots = await db.getBots();
        await botManager.setup(bots)
      } catch (error) {
        logger.error(`Cron job error: ${error}`);
      }
    });
    sh.start();

  } catch (error) {
    logger.error(error);
    await db.close();
    sh?.stop();
    nats.close();
  }
}

run().catch(console.error);
