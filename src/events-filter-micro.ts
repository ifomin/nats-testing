import {logger} from './common/logger'
import {NatsManager} from "./common/nats";
import cron from "node-cron";
import {BotsManager} from "./bots/BotsManager";
import {DB} from "./common/db";
import {Bot, Update} from "../types";

async function run() {
  const nats = new NatsManager();
  const botManager = new BotsManager(nats);

  const db = new DB();
  let sh;
  try {
    await nats.connect()
    logger.info('nats connected');

    await db.connect();
    logger.info('db connected');


    // Cache to store the last timestamp for each user
    const eventTimestamps = new Map<string, number>();

    const filter = async (key: string, update: Update) => {
      const userKey = `${key}.${update.user}`;
      const currentTime = Date.now();

      // Check if enough time has passed since the last event for this user
      if (eventTimestamps.has(userKey) && currentTime - (eventTimestamps.get(userKey) || 1000) < 1000) {
        logger.info(`Ignoring repeated event for user ${update.user}`);
        return;
      }

      // Update the last timestamp for this user
      eventTimestamps.set(userKey, currentTime);

      // Your filtering logic goes here
      // ...

      logger.info(`Processing event for user ${update.user}: ${update.text}`);
    };


    botManager.on('new_bot', async (bot: Bot)=>{
      await nats.subscribe(`updates.${bot.uid}`, async (update: Update) => {
        logger.info(`subscribe.updates.${bot.uid}> ${update.user} ${update.text}`);
        await filter(`updates.${bot.uid}`, update);
      })
    });


    sh = cron.schedule('*/10 * * * * *', async () => {
      try {
        const bots = await db.getBots();
        await botManager.setup(bots);
      } catch (error) {
        logger.error(`Cron job error: ${error}`);
      }
      });
    sh.start();
  } catch (error) {
    logger.error(error);
    await db.close();
    nats.close();
    sh?.stop();
  }
}

run().catch(console.error);
