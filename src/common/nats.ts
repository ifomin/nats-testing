import {StringCodec, connect, JSONCodec, Msg} from 'nats';
import {natsConfig} from "./config";
import {NatsConnection} from "nats/lib/src/nats-base-client";
import {Update} from "../../types";

export class NatsManager {
  private nc: NatsConnection | null = null;
  private sc: any;

  async connect() {
    this.nc = await connect({ servers: [natsConfig.url ? natsConfig.url : ''] });
    this.sc = JSONCodec();
  }


  async publish<T>(subject: string, data: T): Promise<void> {
    if (!this.nc) {
      throw Error('not connected to nats');
    }
    const message = this.sc.encode(data);
    this.nc?.publish(subject, message);
  }



  async subscribe<TypeOfData>(subject: string, callback: (data: TypeOfData) => void) {
    if (!this.nc) {
      throw Error('not connected to nats');
    }

    return this.nc.subscribe(subject, {
      callback: (err, msg: Msg) => {
        const data = this.sc.decode(msg.data);
        callback(data);
      },
    });
  }



  close(): void {
    this.nc?.close();
  }
}