import "reflect-metadata"
import {dbConfig} from './config';
import {Bot} from "../../types";
import {DataSource} from "typeorm";
import {BotEntity} from "../entities/Bot";
import {logger} from "./logger";
import {ImageEntity} from "../entities/Image";

export class DB {
  private dataSource: DataSource;

  constructor() {

  }

  async connect() {
    this.dataSource = new DataSource({
      type: 'postgres',
      username: dbConfig.user,
      host: dbConfig.host,
      database: dbConfig.database,
      password: dbConfig.password,
      port: dbConfig.port,
      entities: [
        BotEntity,
        ImageEntity
      ],
      synchronize: true,
    });
    try {
      await this.dataSource.initialize()
      console.log("Data Source has been initialized!")
    } catch  (e) {
      console.log(e);
    }
  }

  async create(bots: Bot[]) {
    try {
      await this.dataSource.transaction(async transactionalEntityManager => {
        await transactionalEntityManager.save(BotEntity, bots);
      });

      return this.getBots();
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  async getBots() {
    return this.dataSource.getRepository(BotEntity).find({ where: { active: true } });
  }


  async close() {
    logger.info('Close db')
  }
}
