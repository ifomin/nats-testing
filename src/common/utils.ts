export const randItemNotPrev = (array: any[], prevs: any[] = []): any | undefined => {
  const filteredArray = array.filter(item => !prevs.includes(item));

  if (filteredArray.length === 0) {
    // If the filtered array is empty, there are no valid options
    return undefined;
  }

  const randomIndex = Math.floor(Math.random() * filteredArray.length);
  return filteredArray[randomIndex];
};