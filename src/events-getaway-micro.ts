import {logger} from './common/logger'
import {NatsManager} from "./common/nats";
import cron from "node-cron";
import {BotsManager} from "./bots/BotsManager";
import {DB} from "./common/db";

async function run() {
  const nats = new NatsManager();
  const botManager = new BotsManager(nats);

  const db = new DB();
  let sh;
  try {
    await nats.connect()
    logger.info('nats connected');

    await db.connect();
    logger.info('db connected');

    sh = cron.schedule('*/10 * * * * *', async () => {
      try {
        const bots = await db.getBots();
        await botManager.setup(bots.map(it=>({...it, pulling: true})));
      } catch (error) {
        logger.error(`Cron job error: ${error}`);
      }
      });
    sh.start();
  } catch (error) {
    logger.error(error);
    await db.close();
    nats.close();
    sh?.stop();
  }
}

run().catch(console.error);
