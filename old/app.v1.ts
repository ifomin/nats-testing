import {MessageContext, VK} from 'vk-io';
import cron from 'node-cron';
import {DB} from "../src/common/db";
import {logger} from '../src/common/logger'
import {vkConfig} from "../src/common/config";
import {Bot, Update, UpdateType} from "../types";
import {NatsManager} from "../src/common/nats";

async function run() {
  let bots: VK[] = [];
  const nats = new NatsManager();
  const publish = async (uid: string, update: Update) => {
    await nats.publish<Update>(`updates_${uid}`, update)
    logger.info(`${uid}> ${update.type} update ${JSON.stringify(update)}`);
  }

  const setupBot = async (bot: Bot) => {
    const vk= new VK({
      token: bot.token,
      pollingGroupId: +bot.group_id,
      apiMode: 'parallel',
    });
    vk.updates
      .on('wall_repost', async (ctx) => {
        if (!ctx.isRepost || ctx.type !== 'wall_post') {
          return;
        }
        const copyHistory = ctx.wall.copyHistory;
        if (!copyHistory) {
          return;
        }
        const postId = copyHistory[0].id;
        const isPost = copyHistory[0].postType === 'post';

        if (!postId || !isPost) {
          return;
        }

        await publish(bot.uid, {
          user: ctx.wall.ownerId,
          type: UpdateType.repost,
          object: postId,
        });
      })
      .on('message_new', async (ctx) => {
        if (ctx.is(['message'])) {
          if (ctx.isOutbox || ctx.isFromGroup) {
            return;
          }
        }
        await publish(bot.uid, {
          user: ctx.senderId,
          type: UpdateType.message,
          text: ctx.text,
        });
      })
      .on('like_add', async (ctx: MessageContext) => {
        if (ctx.objectType !== 'post' || ctx.objectId < bot.from_id) {
          return;
        }
        await publish(bot.uid, {
          user: ctx.likerId,
          type: UpdateType.like,
          object: ctx.objectId
        });
      })
      .on('comment', async (ctx) => {
        if (!ctx.isWallComment || ctx.objectId < bot.from_id) {
          return;
        }
        await publish(bot.uid, {
          user: ctx.ownerId,
          type: UpdateType.comment,
          object: ctx.objectId,
          reply_to_comment: ctx.id,
        });
      })
      .startPolling().then(() => {logger.info(`${bot.uid}> Long Polling started`)});
    return vk;
  }

  const db = new DB();
  try {
    await nats.connect()
    logger.info('nats connected');

    await db.connect();
    logger.info('db connected');

    cron.schedule('* * * * *', async ()=>{
      if (bots.length > 0) {
        for (const bot of bots) {
          await bot.updates.stop();
        }
        bots = [];
      }
      const dbBots = await db.getBots();
      logger.info(`bots ${dbBots.length} loaded`);

      for (const bot of dbBots) {
        const b = await setupBot(bot);
        bots.push(b);
      }
    })

  } catch (error) {
    logger.error(error);
    await db.close();
    nats.close();
  }
}

run().catch(console.error);
