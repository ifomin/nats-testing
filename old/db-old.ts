import "reflect-metadata"
import {Pool, PoolClient} from 'pg';
import {dbConfig} from '../src/common/config';
import {Bot} from "../types";

export class DB {
  private pool: Pool;
  private client: PoolClient | null = null;

  constructor() {
    this.pool = new Pool({
      user: dbConfig.user,
      host: dbConfig.host,
      database: dbConfig.database,
      password: dbConfig.password,
      port: dbConfig.port,
    });
  }

  async connect() {
    if (this.client) {
      this.client.release();
    }
    this.client = await this.pool.connect();
  }

  async create(bots: Bot[]) {
    if (!this.client) {
      throw new Error('Not connected to the database');
    }
    await this.client.query('BEGIN');

    const createTableQuery = `
      DROP TABLE bots;
      CREATE TABLE IF NOT EXISTS bots (
        id SERIAL PRIMARY KEY,
        name VARCHAR(255) NOT NULL,
        uid VARCHAR(255) NOT NULL,
        token VARCHAR(255) NOT NULL,
        active BOOLEAN DEFAULT false,
        group_id INT NOT NULL,
        admins TEXT[] NOT NULL,
        from_id INT NOT NULL,
        main_id INT NOT NULL,
      )
    `;
    await this.client.query(createTableQuery);

    try {
      for (const bot of bots) {
        const insertQuery = `
          INSERT INTO bots (name, uid, token, active, group_id, admins, from_id, main_id)
          VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
        `;
        const insertValues = [bot.name, bot.uid, bot.token, bot.active, bot.group_id, bot.admins, bot.from_id, bot.main_id];
        await this.client.query(insertQuery, insertValues);
      }

      await this.client.query('COMMIT');
      return this.getBots();
    } catch (error) {
      console.log(error);
      await this.client.query('ROLLBACK');
      throw error;
    }
  }

  async getBots() {
    if (!this.client) {
      throw new Error('Not connected to the database');
    }
    try {
      const result = await this.client.query('SELECT * FROM bots where active = true');
      const rows: Bot[] = result.rows !== undefined ? result.rows : [];
      return rows;
    } catch (e) {
      return [];
    }
  }
  async getBot(id: string) {
    if (!this.client) {
      throw new Error('Not connected to the database');
    }
    try {
      const result = await this.client.query(`SELECT * FROM bots where id = ${id}`);
      const bot: Bot = result.rows !== undefined ? result.rows[0] : null;
      return bot;
    } catch (e) {
      return null;
    }
  }

  async close() {
    if (!this.client) {
      return;
    }
    this.client.release();
    await this.pool.end();
  }
}
