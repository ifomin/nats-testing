import {Keyboard} from "vk-io";

export interface Bot {
  name: string,
  uid: string,
  group_id: number,
  active: boolean,
  token: string,
  admins: string[],
  from_id: number,
  main_id: number,
  pulling?: boolean,
}

export enum UpdateType {
  like = 'like',
  comment = 'comment',
  message = 'message',
  repost = 'repost',
}

export interface Update {
  type: UpdateType,
  user: number,
  object?: number
  text?: string,
  reply_to_comment?: number,
}

export interface Message {
  user: number,
  text: string,
  object?: number
  keyboard?: string
  attachments?: string,
  dont_parse_link?: boolean,
  random: number,
}

export interface Comment {
  user: number,
  text: string,
  reply_to_comment: number,
  random: number,
  object?: number
  attachments?: string,
}
